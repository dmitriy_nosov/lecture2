package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

/**
 * Проверка выполнения метода при возвращаемом значении функции utilFunc2 == true
 */
public class MyBranchingTest {
    @Test
    public void ifElseExampleSuccessTest() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        boolean result = myBranching.ifElseExample();

        Assertions.assertTrue(result);
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }

    /**
     * Проверка результата выполнения метода
     * если true - вернуть 0
     * если false - большее из значений i1 и i2
     */
    @Test
    public void maxIntTest() {
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);

        int i1 = 11;
        int i2 = 32;

        int result = myBranching.maxInt(i1, i2);

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        System.out.println("utilFunc2 is true, return 0");
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        System.out.println("utilFunc2 is false, return result = " + result);
    }

    /**
     * Проверка результата выполнения метода switchExample с аргументом 1
     */
    @Test
    public void switchExampleTestCase1() {
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        int i = 1;

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsMock, Mockito.atLeast(1)).utilFunc2();
    }

    /**
     * Проверка результата выполнения метода switchExample с аргументом 2
     */
    @Test
    public void switchExampleTestCase2() {
        Utils utilsMock = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsMock);
        int i = 2;

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.never()).utilFunc1("abc");
        Mockito.verify(utilsMock, Mockito.atLeast(1)).utilFunc2();
    }

    /**
     * Проверка результата выполнения метода switchExample с аргументом 0
     *  при utilFunc2 == true
     */
    @Test
    public void switchExampleTestDefaultCaseTrue() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        MyBranching myBranching = new MyBranching(utilsMock);
        int i = 0;

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc2");
    }

    /**
     * Проверка результата выполнения метода switchExample с аргументом 0
     * при utilFunc2 == false
     */
    @Test
    public void switchExampleTestDefaultCaseFalse() {
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsMock);
        int i = 0;

        myBranching.switchExample(i);

        Mockito.verify(utilsMock, Mockito.never()).utilFunc1(Mockito.anyString());
    }
}
